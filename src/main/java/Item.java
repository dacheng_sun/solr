import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;

public class Item implements Serializable {
    @Field
    private String title;
    @Field
    private String id;
    @Field
    private long price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", price=" + price +
                '}';
    }
}
