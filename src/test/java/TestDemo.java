import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TestDemo {
    @Test
    public void testCreateIndex() throws IOException, SolrServerException {
        SolrServer server =new HttpSolrServer("http://localhost:8080/solr/collection1");

        SolrInputDocument document = new SolrInputDocument();

        document.addField("id","111222");
        document.addField("title","山楂熟了11");
        document.addField("price",10011);


        server.add(document);
        server.commit();
    }

    @Test
    public void testCreateIndex2() throws IOException, SolrServerException {
        SolrServer server =new HttpSolrServer("http://localhost:8080/solr/collection1");

        Item item = new Item();
        item.setId("120");
        item.setTitle("来了");
        item.setPrice(1200000);

        server.addBean(item);
        server.commit();
    }
    @Test
    public void testDeleteIndex() throws IOException, SolrServerException {
        SolrServer server =new HttpSolrServer("http://localhost:8080/solr/collection1");

        server.deleteById("110");
        server.commit();
    }
    @Test
    public void testDeleteIndexByQuery() throws IOException, SolrServerException {
        SolrServer server =new HttpSolrServer("http://localhost:8080/solr/collection1");

        server.deleteByQuery("title:来了");
        server.commit();
    }

    /**
     * 演示：使用SolrJ查询索引，返回的是Document形式
     * @throws Exception
     */

    @Test
    public void testQueryDocument() throws Exception {
        // 连接Solr服务器
        HttpSolrServer server = new HttpSolrServer("http://localhost:8080/solr/core2");
        // 创建查询对象：SolrQuery
        SolrQuery query = new SolrQuery("title:iphone");
        // 执行查询,获取响应
        QueryResponse response = server.query(query);

        // 获取查询结果,本质是一个Document的集合
        SolrDocumentList results = response.getResults();
        // 获取总条数
        System.out.println("本次共搜索到" + results.size() + "条数据。");
        // 遍历集合
        for (SolrDocument document : results) {
            System.out.println("id: " + document.getFieldValue("id"));
            System.out.println("titile: " + document.getFieldValue("title"));
            System.out.println("price: " + document.getFieldValue("price"));
        }
    }



    @Test
    public void testQueryBeans() throws Exception {
        // 连接Solr服务器
        HttpSolrServer server = new HttpSolrServer("http://localhost:8080/solr/collection1");
        // 创建查询对象：SolrQuery
        //SolrQuery query = new SolrQuery("title:山楂");
        //SolrQuery query = new SolrQuery("title:山楂OR title:报");
        //SolrQuery query = new SolrQuery("title:山报~2");
        SolrQuery query = new SolrQuery("price:[99 TO 9999]");
        // 执行查询,获取响应
        QueryResponse response = server.query(query);

        // 获取查询结果,指定实体类的类型，返回实体类的集合
        List<Item> list = response.getBeans(Item.class);
        // 打印总条数：
        System.out.println("本次共搜索到" + list.size() + "条数据。");
        // 遍历集合
        for (Item item : list) {
            System.out.println(item);
        }
    }
    @Test
    public void testSortedQuery() throws Exception {
        // 连接Solr服务器
        HttpSolrServer server = new HttpSolrServer("http://localhost:8080/solr/collection1");
        // 创建查询对象：SolrQuery
        SolrQuery query = new SolrQuery("title:山楂");
        // 设置查询的排序参数,参数： 排序的字段名、排序方式
        query.setSort("price", SolrQuery.ORDER.desc);// 这里是按价格降序

        // 执行查询,获取响应
        QueryResponse response = server.query(query);
        // 获取查询结果,指定实体类的类型，返回实体类的集合
        List<Item> list = response.getBeans(Item.class);
        // 打印总条数：
        System.out.println("本次共搜索到" + list.size() + "条数据。");
        // 遍历集合
        for (Item item : list) {
            System.out.println(item);
        }
    }

    public void testPagedQuery() throws Exception {
        // 准备分页参数：
        int currentPage = 2;// 当前页
        final int PAGE_SIZE = 2;// 每页显示条数
        int start = (currentPage - 1) * PAGE_SIZE;// 当前页的起始条数

        // 连接Solr服务器
        HttpSolrServer server = new HttpSolrServer("http://localhost:8080/solr/collection1");
        // 创建查询对象：SolrQuery
        SolrQuery query = new SolrQuery("title:山楂");
        // 设置分页信息到查询对象中
        query.setStart(start);// 设置起始条数
        query.setRows(2);// 设置每页条数

        // 执行查询,获取响应
        QueryResponse response = server.query(query);
        // 获取查询结果,指定实体类的类型，返回实体类的集合
        List<Item> list = response.getBeans(Item.class);
        // 打印搜索结果信息
        System.out.println("当前第" + currentPage + "页，本页共" + list.size() + "条数据。");
// 遍历集合
        for (Item item : list) {
            System.out.println(item);
        }
    }


    // 查询索引并且高亮：
    @Test
    public void testHighlightingQuery() throws Exception {
        // 连接Solr服务器,需要指定地址：我们可以直接从浏览器复制地址。要删除#
        HttpSolrServer server = new HttpSolrServer("http://localhost:8080/solr/collection1");
        // 创建查询对象
        SolrQuery query = new SolrQuery("title:山楂");
        // 设置高亮的标签
        query.setHighlightSimplePre("<em>");
        query.setHighlightSimplePost("</em>");
        // 设置高亮字段
        query.addHighlightField("title");

        // 查询
        QueryResponse response = server.query(query);
        // 解析高亮响应结果,是一个Map
        // 外层的Map：它的key是文档的id,value是一个Map，存放的是这个文档的其它高亮字段
        // 内存的Map：是其它高亮字段，key是其它字段的名称，value是这个字段的值，这个值是一个List
        Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
        // 获取非高亮结果
        List<Item> list = response.getBeans(Item.class);
        for (Item item : list) {
            String id = item.getId();
            System.out.println("id: " + id);
            System.out.println("title: " + highlighting.get(id).get("title").get(0));
            System.out.println("price: " + item.getPrice());
        }
    }

    @Test
    public void testWrite() throws Exception{
        // 创建SolrServer
        CloudSolrServer server = new CloudSolrServer("192.168.206.101:2181,192.168.206.102:2181,192.168.206.103:2181");
        // 指定要访问的Collection名称
        server.setDefaultCollection("collection1");
        //server.setDefaultCollection("myCollection2");

        // 创建Document对象
        SolrInputDocument document = new SolrInputDocument();
        // 添加字段
        document.addField("id", "20");
        document.addField("title", "duang手机，自带特效的手机，值得拥有");

        // 添加Document到Server
        server.add(document);
        // 提交
        server.commit();
    }

    @Test
    public void testDelete() throws Exception{
        // 创建SolrServer
        CloudSolrServer server = new CloudSolrServer("192.168.206.101:2181,192.168.206.102:2181,192.168.206.103:2181");
        // 指定要访问的Collection名称
        server.setDefaultCollection("collection1");
        // 根据ID删除
        server.deleteById("20");
        // 提交
        server.commit();
    }

    @Test
    public void testSearch() throws Exception {
        // 创建SolrServer
        CloudSolrServer server = new CloudSolrServer("192.168.206.101:2181,192.168.206.102:2181,192.168.206.103:2181");
        // 指定要访问的Collection名称
        server.setDefaultCollection("collection1");

        // 查找数据
        QueryResponse response = server.query(new SolrQuery("title:手机"));

        // 以document形式解析数据
        SolrDocumentList documentList = response.getResults();
        // 遍历
        for (SolrDocument solrDocument : documentList) {
            System.out.println(solrDocument.getFieldValue("id"));
            System.out.println(solrDocument.getFieldValue("title"));
        }
    }

}


